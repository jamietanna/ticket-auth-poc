require 'sinatra'
require 'sinatra/reloader' if development?

require_relative './lib'

def missing_parameter(name, logger)
  logger.warn "The parameter `#{name}` was missing from the request, so rejecting it with HTTP 400"
  body = {
    error: 'invalid_request',
    error_description: "The parameter `#{name}` was missing",
  }
  [400, body.to_json]
end

get '/' do
  erb :index
end

post '/ticket' do
  return missing_parameter 'issuer', logger unless params.key? 'issuer'
  return missing_parameter 'ticket', logger unless params.key? 'ticket'
  return missing_parameter 'subject', logger unless params.key? 'subject'

  resources = []
  resources = params['resource[]'] if params.key? 'resource[]'
  resources = [params['resource']] if params.key? 'resource'

  return missing_parameter 'resource' if resources.empty?

  issuer = params['issuer']
  subject = params['subject']
  ticket = params['ticket']

  logger.info "Received request from #{issuer} to provide #{subject} access to #{resources} using ticket #{ticket} "

  profile_config = get_profile_config issuer
  begin
    exchanged = exchange_ticket_for_token profile_config, ticket

    response = {
      response: exchanged,
    }

    logger.info "Successfully exchanged request from from #{issuer} to provide #{subject} access, with response #{exchanged.to_json}"

    [202, response.to_json]
  rescue TokenExchangeError => e
    logger.error e.message

    body = {
      error: 'internal_server_error',
      error_description: 'There was an error exchanging the ticket for a token. More details can be found in the logs'
    }
    return [500, body.to_json]
  end
end
