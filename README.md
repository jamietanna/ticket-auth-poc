# IndieAuth Ticket Auth (Proof of Concept)

A simple Sinatra application to prove out an [IndieAuth Ticket Auth](https://indieweb.org/IndieAuth_Ticket_Auth) receiver endpoint.

## Usage

```sh
bundle config set --local path 'vendor/bundle'
bundle install
bundle exec rackup
# app is now running at http://localhost:9292
```
