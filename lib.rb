require 'json'
require 'net/http'
require 'microformats'

class TokenExchangeError < StandardError
end

def get(url)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true if uri.instance_of? URI::HTTPS
  request = Net::HTTP::Get.new(uri.request_uri)
  http.request(request)
end

def post(url, params)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true if uri.instance_of? URI::HTTPS
  request = Net::HTTP::Post.new(uri.request_uri)
  request.set_form_data params
  http.request(request)
end

def get_profile_config(profile_url)
  res = get(profile_url)

  parsed = Microformats.parse(res.body)
  {
    profile_url: profile_url,
    authorization_endpoint: parsed.rels['authorization_endpoint'][0],
    token_endpoint: parsed.rels['token_endpoint'][0],
  }
end

def exchange_ticket_for_token(profile_config, ticket)
  params = {
    grant_type: 'ticket',
    ticket: ticket,
  }
  response = post(profile_config[:token_endpoint], params)

  raise TokenExchangeError.new "Token endpoint rejected the ticket auth request: HTTP #{response.code}, Body: #{response.body}" unless response.kind_of? Net::HTTPSuccess

  return JSON.parse response.body
end
